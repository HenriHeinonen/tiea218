﻿// TÄYSIN UUSI

window.onload = function() {
  var head = document.getElementsByTagName("head")[0];
  var h1 = document.getElementsByTagName("h1")[0];
  if (h1.textContent == "Breakthrough") {
    //muutaTaulukonkokoa();
    var style = document.createElement("style");
    head.appendChild(style);
    //var styleText = document.createTextNode('td {min-width: 10px; min-height: 10px; width: 10px; height: 10px; border: 1px solid black; margin: 0; padding: 0;} table {border-spacing: 0; border-collapse: collapse; width: 100%; height: 100%;} .musta {background-color: black; color: white;} span {margin: 5px; padding: 0; min-width: 50px; min-height: 50px; display: block; border-radius: 50%;} span.blue {background: radial-gradient(ellipse at center, #f0f9ff 0%, #cbebff 47%, #a1dbff 100%);} span.red {background: radial-gradient(ellipse at center, #fff9f0 0%, #ffebcb 47%, #ffdba1 100%);}');
    var styleText = document.createTextNode('td {min-width: 10px; min-height: 10px; width: 10px; height: 10px; border: 1px solid black; margin: 0; padding: 0;} table {border-spacing: 0; border-collapse: collapse;} .musta {background-color: black; color: white;} span {margin: 5px; padding: 0; min-width: 50px; min-height: 50px; display: block; border-radius: 50%;} span.blue {background: radial-gradient(ellipse at center, #f0f9ff 0%, #cbebff 47%, #a1dbff 100%);} span.red {background: radial-gradient(ellipse at center, #fff9f0 0%, #ffebcb 47%, #ffdba1 100%);}');
    style.appendChild(styleText);
    var p = document.createElement("p");
    p.textContent = "Kerro luotavan ruudukon koko. Ruudukko on yhtä monta ruutua leveä kuin korkea. ";
    h1.parentNode.insertBefore(p, h1.nextSibling);
    var form = document.createElement("form");
    form.setAttribute("action","");
    form.setAttribute("id","ruudukko");
    p.parentNode.insertBefore(form, p.nextSibling);
    var fieldset = document.createElement("fieldset");
    form.appendChild(fieldset);
    var p2 = document.createElement("p");
    fieldset.appendChild(p2);
    var label = document.createElement("label");
    p2.appendChild(label);
    var inputx = document.createElement("input");
    inputx.setAttribute("type","text");
    inputx.setAttribute("name","x");
    label.appendChild(inputx);
    var textLeveys = document.createTextNode("Leveys ");
    label.insertBefore(textLeveys, label.childNodes[0]);
    var p3 = document.createElement("p");
    form.appendChild(p3);
    var inputLuo = document.createElement("input");
    inputLuo.setAttribute("type","submit");
    inputLuo.setAttribute("value","Luo");
    p3.appendChild(inputLuo);

    var pelitaulu = document.createElement("table");
    form.parentNode.insertBefore(pelitaulu, form.nextSibling);
  }

  var kentta = document.getElementsByTagName("input")[0];
  kentta.value = 8;

  var muisti = 0;
  //var muisti2 = 0;
  var nappula = document.getElementsByTagName("input")[1];
  nappula.addEventListener('click', luoRuudukko, false);

  var td = document.getElementsByTagName("td");
  for (var i=0; i<td.length; i++) {
    td[i].style.width = "61px";
    td[i].style.height = "61px";
    td[i].addEventListener("click", siirraNappulaa, false);
  }

  var span = document.getElementsByTagName("span");
  for (var i=0; i<span.length; i++) {
    span[i].addEventListener("click", laitaMuistiin, false);
  }

  nappula.click();

}

function luoRuudukko(e) {
  e.preventDefault();
  var leveys = document.getElementsByTagName("input")[0];
  if ((leveys.value >= 8) && (leveys.value <= 32)) {
    var vanhataulukko=document.getElementsByTagName("table")[0];
    var taulukko=document.createElement("table");
    for (j = 0; j < leveys.value; j++) {
      var tr = document.createElement("tr");
      for (i = 0; i < leveys.value; i++) {
        if ((i % 2 === 0) && (j % 2 === 0)) {
          var td = document.createElement("td");
          td.style.width = "61px";
          td.style.height = "61px";
          tr.appendChild(td);
          td.addEventListener("click", siirraNappulaa, false);
          if ((j === 0) || (j === 1)) {
            var spanred = document.createElement("span");
            spanred.setAttribute("class","red");
            spanred.setAttribute("id","red_"+j+"_"+i);
            spanred.addEventListener("click", laitaMuistiin, false);
            td.appendChild(spanred);
          }
          else if ((j === leveys.value-2) || (j === leveys.value-1)) {
            var spanblue = document.createElement("span");
            spanblue.setAttribute("class","blue");
            spanblue.setAttribute("id","blue_"+j+"_"+i);
            spanblue.addEventListener("click", laitaMuistiin, false);
            td.appendChild(spanblue);
          }
        }
        else if ((i % 2 != 0) && (j % 2 != 0)) {
          var td = document.createElement("td");
          td.style.width = "61px";
          td.style.height = "61px";
          tr.appendChild(td);
          td.addEventListener("click", siirraNappulaa, false);
          if ((j === 0) || (j === 1)) {
            var spanred = document.createElement("span");
            spanred.setAttribute("class","red");
            spanred.setAttribute("id","red_"+j+"_"+i);
            spanred.addEventListener("click", laitaMuistiin, false);
            td.appendChild(spanred);
          }
          else if ((j === leveys.value-2) || (j === leveys.value-1)) {
            var spanblue = document.createElement("span");
            spanblue.setAttribute("class","blue");
            spanblue.setAttribute("id","blue_"+j+"_"+i);
            spanblue.addEventListener("click", laitaMuistiin, false);
            td.appendChild(spanblue);
          }
        }
        else {
          var tdmusta = document.createElement("td");
          tdmusta.setAttribute("class","musta");
          tdmusta.style.width = "61px";
          tdmusta.style.height = "61px";
          tr.appendChild(tdmusta);
          tdmusta.addEventListener("click", siirraNappulaa, false);
          if ((j === 0) || (j === 1)) {
            var spanred = document.createElement("span");
            spanred.setAttribute("class","red");
            spanred.setAttribute("id","red_"+j+"_"+i);
            spanred.addEventListener("click", laitaMuistiin, false);
            tdmusta.appendChild(spanred);
          }
          else if ((j === leveys.value-2) || (j === leveys.value-1)) {
            var spanblue = document.createElement("span");
            spanblue.setAttribute("class","blue");
            spanblue.setAttribute("id","blue_"+j+"_"+i);
            spanblue.addEventListener("click", laitaMuistiin, false);
            tdmusta.appendChild(spanblue);
          }
        }
        taulukko.appendChild(tr);
      } //...for (i = 0; i < leveys.value; i++) {
    } // ...for (j = 0; j < leveys.value; j++) {
    vanhataulukko.parentNode.replaceChild(taulukko, vanhataulukko);
  } // ...if ((leveys.value >= 8) && (leveys.value <= 32)) {
} // ...function luoRuudukko(e)

function laitaMuistiin(e) {
  e.stopPropagation();
  muisti = this.id;
  //muisti2 = this.parentNode;
  //alert(muisti);
  this.style.background = "radial-gradient(red, green, blue)";
}

function siirraNappulaa(e) {
  e.stopPropagation();
  klikkipaikka = this;
  //alert(klikkipaikka);
  var nappula = document.getElementById(muisti);
  if ((nappula.className == "red") && (klikkipaikka.childNodes.length == 0)) {
    //alert(klikkipaikka.childNodes.length);
    nappula.parentNode.removeChild(nappula);
    var span = document.createElement("span");
    span.setAttribute("class","red");
    span.setAttribute("id",muisti);
    span.addEventListener("click", laitaMuistiin, false);
    klikkipaikka.appendChild(span);
    muisti = 0;
  }
  else if ((nappula.className == "blue") && (klikkipaikka.childNodes.length == 0)) {
    nappula.parentNode.removeChild(nappula);
    var span = document.createElement("span");
    span.setAttribute("class","blue");
    span.setAttribute("id",muisti);
    span.addEventListener("click", laitaMuistiin, false);
    klikkipaikka.appendChild(span);
    muisti = 0;
  }
}

/*
function muutaTaulukonkokoa() {
  setInterval(function(){ var taulukko = document.getElementsByTagName("table")[0]; taulukko.setAttribute("height",taulukko.style.offsetWidth); }, 3000);
}
*/