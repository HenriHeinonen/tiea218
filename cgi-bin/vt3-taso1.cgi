#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgitb
#cgitb.enable()
import cgi

print """Content-type: text/html;charset=UTF-8

<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fi" lang="fi">
<head>
<title>Ruudukko</title>
<link rel="StyleSheet" href="tyylit.css" type="text/css"  />
</head>
<body>

<h1>Ruudukko</h1>

<p>Kerro luotavan shakkiruudukon koko. Ruudukko on yhtä monta ruutua 
leveä kuin korkea. </p>
"""
#print "Hello World!"
print """
<form action="" method="get">
<fieldset>
<p><label>Leveys <input value="10" type="text" name="x" /></label></p>
<p><label>Teksti <input value="kukku" type="text" name="teksti" /></label></p>
</fieldset>
<p><input type="submit" value="Luo" /></p>
</form>
"""

#laskuri = 0
form = cgi.FieldStorage()

leveys = form.getfirst("x", "")
teksti = form.getfirst("teksti", "")
#print leveys
#print teksti

if ( (int(leveys) >= 6) and (int(leveys) <= 12) ):
  print """
  <table>
  """
  for j in range(0, int(leveys)):
    print """
    <tr>
    """
    for i in range(0, int(leveys)):
      if ((i % 2 == 0) and (j % 2 == 0)):
        print """
        <td>
        """
        print cgi.escape(teksti, quote=True)
        print """
        </td>
        """
      elif ((i % 2 != 0) and (j % 2 != 0)):
        print """
        <td>
        """
        print cgi.escape(teksti, quote=True)
        print """
        </td>
        """
      else:
        print """
        <td class="musta">
        """
        print cgi.escape(teksti, quote=True)
        print """
        </td>
        """
    print """
    </tr>
    """
  print """
  </table>
  """
else:
  print """
  <p>Taulukon koon tulee olla vähintään 6, mutta korkeintaan 12.<p>
  """

print """
</body>
</html>
"""