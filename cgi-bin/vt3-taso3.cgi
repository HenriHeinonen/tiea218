#!/usr/bin/python
# -*- coding: utf-8 -*-

import cgitb
cgitb.enable()
import cgi
from xml.dom import minidom
import xml.dom.minidom
xmldoc = minidom.parse('vt3-taso3.html')
form = cgi.FieldStorage()
if ( form.getvalue("x") is None ):
  print "Content-type: text/html;charset=UTF-8\n"
  document = xmldoc.toprettyxml(indent="  ", encoding="utf-8")
  #document.rstrip('\n')
  #sansfirstline = '\n'.join(document.split('\n')[1:])
  print document
  #print sansfirstline
else:
  print "Content-type: text/html;charset=UTF-8\n"
  document = xmldoc.toprettyxml(indent="  ", encoding="utf-8")
  dom = xml.dom.minidom.parseString(document)
  leveys = form["x"].value
  teksti = form["teksti"].value
  if ( (int(leveys) >= 6) and (int(leveys) <= 12) ):
    taulukko = dom.getElementsByTagName("table")[0]
    poistettava = dom.getElementsByTagName("tr")[0]
    poistettava.parentNode.removeChild(poistettava)
    #text = dom.createTextNode('Some textual content.')
    #taulukko.appendChild(text)
    for j in range(0, int(leveys)):
      tr = dom.createElement("tr")
      taulukko.appendChild(tr)
      for i in range(0, int(leveys)):
        if ((i % 2 == 0) and (j % 2 == 0)):
          td = dom.createElement("td")
          tr.appendChild(td)
          if ((j == 0) or (j == 1)):
            spanred = dom.createElement("span")
            spanred.setAttribute("class","red")
            td.appendChild(spanred)
          elif ((j == int(leveys)-2) or (j == int(leveys)-1)):
            spanblue = dom.createElement("span")
            spanblue.setAttribute("class","blue")
            td.appendChild(spanblue)
          else:
            text = dom.createTextNode(cgi.escape(teksti, quote=True))
            td.appendChild(text)
        elif ((i % 2 != 0) and (j % 2 != 0)):
          td = dom.createElement("td")
          tr.appendChild(td)
          if ((j == 0) or (j == 1)):
            spanred = dom.createElement("span")
            spanred.setAttribute("class","red")
            td.appendChild(spanred)
          elif ((j == int(leveys)-2) or (j == int(leveys)-1)):
            spanblue = dom.createElement("span")
            spanblue.setAttribute("class","blue")
            td.appendChild(spanblue)
          else:
            text = dom.createTextNode(cgi.escape(teksti, quote=True))
            td.appendChild(text)
        else:
          tdmusta = dom.createElement("td")
          #tdmusta = dom.createAttribute("musta")
          tdmusta.setAttribute("class","musta")
          tr.appendChild(tdmusta)
          if ((j == 0) or (j == 1)):
            spanred = dom.createElement("span")
            spanred.setAttribute("class","red")
            tdmusta.appendChild(spanred)
          elif ((j == int(leveys)-2) or (j == int(leveys)-1)):
            spanblue = dom.createElement("span")
            spanblue.setAttribute("class","blue")
            tdmusta.appendChild(spanblue)
          else:
            text = dom.createTextNode(cgi.escape(teksti, quote=True))
            tdmusta.appendChild(text)
  print dom.toprettyxml(indent="  ", encoding="utf-8")